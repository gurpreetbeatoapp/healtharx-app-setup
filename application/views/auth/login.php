<h1><?php //echo lang('login_heading');?></h1>
<p><?php// echo lang('login_subheading');?></p>

<div id="infoMessage"><?php //echo $message;?></div>

<?php // echo form_open("auth/login");?>

  <p>
    <?php // echo lang('login_identity_label', 'identity');?>
    <?php // echo form_input($identity);?>
  </p>

  <p>
    <?php // echo lang('login_password_label', 'password');?>
    <?php // echo form_input($password);?>
  </p>

  <p>
    <?php // echo lang('login_remember_label', 'remember');?>
    <?php // echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </p>


  <p><?php // echo form_submit('submit', lang('login_submit_btn'));?></p>

<?php //echo form_close();?>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title><?php echo env('APP_NAME') ?> | Login </title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
  
 </head>
 <body>
  <div class="container">
   <br />
   <h2 align="center">Login using Google Account</h2>
   <br />
   <div class="panel panel-default">
   <?php
   if(!isset($login_button))
   {

    $user_data = $this->session->userdata('user_data');
    echo '<div class="panel-heading">Welcome User</div><div class="panel-body">';
    echo '<img src="'.$user_data['profile_picture'].'" class="img-responsive img-circle img-thumbnail" />';
    echo '<h3><b>Name : </b>'.$user_data["first_name"].' '.$user_data['last_name']. '</h3>';
    echo '<h3><b>Email :</b> '.$user_data['email_address'].'</h3>';
    echo '<h3><a href="'.base_url().'google_login/logout">Logout</h3></div>';
   }
   else
   {
    echo '<div align="center">'.$login_button . '</div>';
   }
   ?>
   </div>
   <p><a href="forgot_password" style="margin-left: 490px;"><?php echo lang('login_forgot_password');?></a></p>
  </div>
 </body>
</html>

